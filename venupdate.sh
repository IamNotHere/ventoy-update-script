#!/usr/bin/env bash

URL="https://api.github.com/repos/ventoy/Ventoy/releases/latest"
BASE_DIR="$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)")"

if ! ping -q -c 1 -W 1 "$(echo "$URL" | awk -F[/:] '{print $4}')" > /dev/null; then
  echo "No internet connection"
  exit
fi

function dependency_check() {
	local PACKAGES=($*)
	local MISSING=""
	for i in "${PACKAGES[@]}"; do
		if [ "$(command -v "$i")" == "" ]; then
			MISSING="$MISSING $i"
		fi
	done
	if [ "$MISSING" != "" ]; then
		if [ "$(echo "$MISSING" | wc -w)" == "1" ]; then
			echo "Missing package:$MISSING"
		else
			echo "Missing packages:$MISSING"
		fi
		exit
	fi
}
dependency_check curl wget tar pv sha256sum

cd "$BASE_DIR" || exit

if [ -d dwl ]; then
	rm -rf dwl
fi

if [ -d tmp ]; then
	rm -rf tmp
	mkdir tmp
else
	mkdir tmp
fi
cd tmp || exit

LINKS="$(curl -s "$URL" | grep browser_download_url | sed -re 's/.*: "([^"]+)".*/\1/')"

echo -e "===== Downloading files ====="
wget -q -- "$(echo "$LINKS" | grep sha256.txt)"
wget -q -- "$(echo "$LINKS" | grep linux)"

echo -e "===== Veryfying files ======="
if sha256sum --ignore-missing -c sha256.txt > /dev/null; then
	echo "All files OK"
else
	echo "Downloaded files are corrupted"
	exit
fi

echo -e "===== Extracting files ======"
du -h ventoy*linux.tar.gz
pv ventoy*linux.tar.gz | tar -xz

echo -e "===== Moving files =========="
mkdir "$BASE_DIR"/dwl
mv ventoy*/* "$BASE_DIR"/dwl

echo -e "===== Cleaning up ==========="
cd "$BASE_DIR" || exit
if [ -d tmp ]; then
	rm -rf tmp
fi

echo -e "===== Starting up ==========="
cd dwl || exit

if [ -n "$DISPLAY" ]; then
	echo "Graphics server detected, running GUI"
	./VentoyGUI."$(uname -m)" > /dev/null
else
	echo "Graphics server not detected, running TUI"
	./Ventoy2Disk.sh
fi

